local metadataLib = import "../utils/metadata.libsonnet";
local apps = import "../utils/apps.jsonnet";
local routableService = import "../utils/routableService.jsonnet";

local filePath(path) = 
  local pathSplit = std.split(path, '/');
  local pathLength = std.length(pathSplit);
  if (pathLength>0) then pathSplit[pathLength-1] else "";

local newStaticContentNginxContainer(containerImage, containerPort, metadata, rewriteTarget, secretPath, secretFilePath) = {
  name: "nginx",
  image: containerImage,
  imagePullPolicy: "Always",
  ports: [
    {
      containerPort: containerPort,
    },
  ],
  livenessProbe: {
    httpGet: {
      path: if (rewriteTarget == "/") then "/_livenessProbe" else rewriteTarget + "/_livenessProbe",
      port: containerPort,
    },
    initialDelaySeconds: 10,
  },
  resources:
    if (metadata.labels[metadataLib.labelsKeys.deployKind] == metadataLib.deployKind.productionDeploy) then {
      limits: {
        cpu: "500m",
        memory: "128Mi",
      },
      requests: {
        cpu: "100m",
        memory: "64Mi",
      },
    } else {
      limits: {
        cpu: "40m",
        memory: "128Mi",
      },
      requests: {
        cpu: "20m",
        memory: "32Mi",
      },
    },
  [if secretPath != "" then "volumeMounts"]:
  [
    {
      "name": metadata.labels[metadataLib.labelsKeys.appName] + "-authbasic-secret",
      "mountPath": secretPath,
      [if secretFilePath != "" then "subPath"]: secretFilePath
    }
  ]

};

local newDeployment(metadata, containerImage, targetHostname, containerPort, routePath = "/", rewriteTarget = "/", secretPath) = {
  deployment: apps.newDeploymentTemplate(metadata) {
    spec+: {
      template+: {
        spec+: {
          containers: [
            newStaticContentNginxContainer(containerImage, containerPort, metadata, rewriteTarget, secretPath, filePath(secretPath))
          ],
          [if secretPath != "" then "volumes"]:
          [
            {
              name: metadata.labels[metadataLib.labelsKeys.appName] + "-authbasic-secret",
              secret:{
                secretName: metadata.labels[metadataLib.labelsKeys.appName] + "-authbasic-secret"
              },
            }
          ]
        },
      },
    },
  },
} + routableService.newRoutableHTTPService(targetHostname,
    metadata, containerPort, routePath, rewriteTarget)
;

{
  newDeployment:: newDeployment
}
