
local metadataPrefix = "preview.cbi.eclipse.org";

local deployKind = {
  productionDeploy: "productionDeploy",
  branchDeploy: "branchDeploy",
  changeRequestPreview: "changeRequestPreview",
};

local labelsKeys = {
  appName:  "%s/%s" % [metadataPrefix, "app-name"],
  branchName: "%s/%s" % [metadataPrefix, "branch-name"],
  deployKind: "%s/%s" % [metadataPrefix, "deploy-kind"],
};

local annotationsKeys = {
  repoHost: "%s/%s" % [metadataPrefix, "repo-host"],
  repoId: "%s/%s" % [metadataPrefix, "repo-id"],
  changeId: "%s/%s" % [metadataPrefix, "change-id"],
  botId: "%s/%s" % [metadataPrefix, "bot-id"],
  lastDeployDateTime: "%s/%s" % [metadataPrefix, "last-deploy-datetime"],
};

local newLabels(appName, branchName, deployKind) = {
  [labelsKeys.appName]: appName, # e.g., main
  [labelsKeys.branchName]: branchName, # e.g., main
  [labelsKeys.deployKind]: deployKind,
};

local newAnnotations(repoHost, repoId, botId, lastDeployDateTime, changeId = null) = {
  [annotationsKeys.repoHost]: repoHost, # e.g., github.com
  [annotationsKeys.repoId]: repoId, # e.g., eclipsefdn/abcd
  [annotationsKeys.lastDeployDateTime]: lastDeployDateTime,
} + if (botId != null && botId != "") then {
  [annotationsKeys.botId]: botId,
} else {
} + if (changeId != null && changeId != "") then {
  [annotationsKeys.changeId]: changeId,
} else {
};

local newMetadata(appName, name, namespace, branchName, deployKind, botId, lastDeployDateTime, repoHost, repoId, changeId = null) = {
  name: name,
  namespace: namespace,
  labels: newLabels(appName, branchName, deployKind),
  annotations: newAnnotations(repoHost, repoId, botId, lastDeployDateTime, changeId),
};

{
  deployKind: deployKind,
  labelsKeys: labelsKeys,
  annotationsKeys: annotationsKeys,

  newLabels:: newLabels,
  newAnnotations:: newAnnotations,
  newMetadata:: newMetadata,
}