local newRoutableHTTPService(hostname, metadata, targetPort, routePath = "/", rewriteTarget = "/") = {
  local serviceName = metadata.name,
  local portName = "http",
  service: {
    apiVersion: "v1",
    kind: "Service",
    metadata: metadata {
      name: serviceName,
    },
    spec: {
      ports: [
        {
          name: portName,
          port: 80,
          protocol: "TCP",
          targetPort: targetPort,
        },
      ],
      selector: metadata.labels,
    },
  },
  route: {
    apiVersion: "route.openshift.io/v1",
    kind: "Route",
    metadata: metadata {
      annotations+: {
        "haproxy.router.openshift.io/timeout": "10s",
        "haproxy.router.openshift.io/disable_cookies": "true",
        "haproxy.router.openshift.io/balance": "roundrobin",
        "haproxy.router.openshift.io/rewrite-target": rewriteTarget,
      },
    },
    spec: {
      host: hostname,
      path: routePath,
      port: {
        targetPort: portName,
      },
      tls: {
        termination: "edge",
        insecureEdgeTerminationPolicy: "Allow",
      },
      to: {
        kind: "Service",
        name: serviceName,
        weight: 100,
      },
    },
  },
};

{
  newRoutableHTTPService:: newRoutableHTTPService
}