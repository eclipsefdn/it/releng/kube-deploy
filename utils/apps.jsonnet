local metadataLib = import "metadata.libsonnet";
local newDeploymentTemplate(metadata) = {
  apiVersion: "apps/v1",
  kind: "Deployment",
  metadata: metadata,
  spec: {
    selector: {
      matchLabels: metadata.labels,
    },
    replicas: if (metadata.labels[metadataLib.labelsKeys.deployKind] == metadataLib.deployKind.productionDeploy) then 2 else 1,
    template: {
      metadata: {
        labels: metadata.labels,
      },
      spec: {
        affinity: {
          nodeAffinity: {
            preferredDuringSchedulingIgnoredDuringExecution: [
              {
                preference: {
                  matchExpressions: [
                    {
                      key: "speed",
                      operator: "NotIn",
                      values: [ "fast" ],
                    },
                  ],
                },
                weight: 1
              },
            ],
          },
        },
        topologySpreadConstraints: [
          {
            maxSkew: 1,
            topologyKey: "kubernetes.io/hostname",
            whenUnsatisfiable: "DoNotSchedule",
            labelSelector: {
              matchLabels: metadata.labels,
            },
          },
        ],
      },
    },
  },
};

{
  newDeploymentTemplate:: newDeploymentTemplate,
}